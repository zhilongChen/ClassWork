import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class Example16 {

	private static AbstractButton btn1;

	/**
	 * @param args
	 */
	@SuppressWarnings("null")
	public static void main(String[] args) {
		JButton bth1 = new JButton("模态对话框");
		JButton bth2 = new JButton("非模态对话框");
		JFrame f = new JFrame("DialogDemo");
		
		f.setSize(300,250);
		f.setLocation(300,200);
		f.setLayout(new FlowLayout());
		
		f.add(bth1);
		f.add(bth2);
		
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
		
		final JDialog dialog = new JDialog(f,"dialog");
		JButton bth3 = new JButton("确定");
		final JLabel label = new JLabel();
		dialog.setLayout(new FlowLayout());
		dialog.add(bth3);
		dialog.add(label);
		dialog.setSize(220,150);
		dialog.setLocation(350,250);
		
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dialog.setModal(true);
				if (dialog.getComponents().length == 1) {
					dialog.add(label);
				}
				label.setText("模式对话框，点击确定按钮关闭");
				dialog.setVisible(true);
			}
		});
		AbstractButton btn2 = null;
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dialog.setModal(false);
				if (dialog.getComponents().length == 1) {
					dialog.add(label);
				}
				label.setText("非模式对话框，点击确定按钮关闭");
				dialog.setVisible(true);
			}
		});
		AbstractButton btn3 = null;
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dialog.dispose();
			}
		});
	}
}
