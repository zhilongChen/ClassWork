import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
public class Example20 extends JFrame {
	private static final long serialVersionUID = 1L;
	private ButtonGroup group; 
	private JPanel panel; 
	private JPanel pallet;
	public Example20() {
		pallet = new JPanel();
		this.add(pallet, BorderLayout.CENTER); 
		panel = new JPanel();
		group = new ButtonGroup();
		addJRadioButton("��");
		addJRadioButton("��");
		addJRadioButton("��");
		this.add(panel, BorderLayout.SOUTH);
		this.setSize(300, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	private void addJRadioButton(final String text) {
		JRadioButton radioButton = new JRadioButton(text);
		group.add(radioButton);
		panel.add(radioButton);
		radioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color color = null;
				if ("��".equals(text)) {
					color = Color.GRAY;
				} else if ("��".equals(text)) {
					color = Color.PINK;
				} else if ("��".equals(text)) {
					color = Color.YELLOW;
				} else {
					color = Color.WHITE;
				}
				pallet.setBackground(color);
			}
		});
	}

	public static void main(String[] args) {
		new Example20();
	}
}
