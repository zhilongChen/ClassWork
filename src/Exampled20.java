import java.awt.BorderLayout;
 	
 	import javax.swing.ButtonGroup;
 	import javax.swing.JFrame;
 	import javax.swing.JPanel;
 	import javax.swing.JRadioButton;
 	
 	public class Example20 extends JFrame{
 		//利用成员变量的方式来，保存界面上的控件
 		private JPanel panel;
 		private JPanel pallet;
 		private ButtonGroup group;
 		private JRadioButton yellowButton;
 		private JRadioButton grayButton;
 		private JRadioButton redButton;
 		
 		public Example20() {
 			
 			panel = new JPanel();
 			pallet = new JPanel();
 			group = new ButtonGroup();
 			yellowButton = new JRadioButton("黄");
 			grayButton = new JRadioButton("灰");
 			redButton = new JRadioButton("红");
 			
 			this.setLayout(new BorderLayout());
 			this.add(pallet,BorderLayout.CENTER);//讲pallet加入正中间
 			
 			panel.add(yellowButton);
 			panel.add(grayButton);
 			panel.add(redButton);
 			this.add(panel,BorderLayout.SOUTH);//将panel加到南边
 			
 			this.setSize(300, 300);
 			this.setDefaultCloseOperation(EXIT_ON_CLOSE);
 			this.setVisible(true);
 			
 		}
 		
 		public static void main(String[] args) {
 			
 			new Example20();
 			
 			
 		}
 	
 	}
